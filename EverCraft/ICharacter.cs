﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft
{
    public interface ICharacter
    {
        string Name { get; set; }
        CharacterAlignment Alignment { get; set; }
        int ArmorClass { get; set; }
        int HitPoints { get; set; }
        ICharacterAbility Strength { get; set; }
        ICharacterAbility Dexterity { get; set; }
        ICharacterAbility Constitution { get; set; }
        ICharacterAbility Wisdom { get; set; }
        ICharacterAbility Intelligence { get; set; }
        ICharacterAbility Charisma { get; set; }
        bool IsDead { get; }

        bool Attack(int attackValue, ICharacter character);
    }
}
