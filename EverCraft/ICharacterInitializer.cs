﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft
{
    public interface ICharacterInitializer
    {
        void Initialize(ICharacter character);
    }
}
