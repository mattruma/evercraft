﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft
{
    public class CharacterInitializer : ICharacterInitializer
    {
        public void Initialize(ICharacter character)
        {
            character.Name = string.Empty;
            character.Alignment = CharacterAlignment.Good;
            character.ArmorClass = 10;
            character.HitPoints = 5;

            character.Strength = new CharacterAbility(10);
            character.Dexterity = new CharacterAbility(10);
            character.Constitution = new CharacterAbility(10);
            character.Wisdom = new CharacterAbility(10);
            character.Intelligence = new CharacterAbility(10);
            character.Charisma = new CharacterAbility(10);
        }
    }
}
