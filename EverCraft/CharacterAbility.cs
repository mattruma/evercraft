﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft
{
    public class CharacterAbility : ICharacterAbility
    {
        private int _value;
        public int Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (value < 1)
                    throw new ArgumentOutOfRangeException();

                if (value > 20)
                    throw new ArgumentOutOfRangeException();

                _value = value;
            }
        }
        public int Modifier
        {
            get { return this.Value / 2 - 5; }
        }

        public CharacterAbility()
            : this(10)
        {
        }

        public CharacterAbility(int value)
        {
            this.Value = value;
        }
    }
}
