﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft
{
    public interface ICharacterAbility
    {
        int Value { get; set; }
        int Modifier { get; }
    }
}
