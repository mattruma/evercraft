﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft
{
    public class Character : ICharacter
    {
        private readonly ICharacterInitializer _characterInitializer;

        public string Name { get; set; }
        public CharacterAlignment Alignment { get; set; }
        public int ArmorClass { get; set; }
        public int HitPoints { get; set; }
        public ICharacterAbility Strength { get; set; }
        public ICharacterAbility Dexterity { get; set; }
        public ICharacterAbility Constitution { get; set; }
        public ICharacterAbility Wisdom { get; set; }
        public ICharacterAbility Intelligence { get; set; }
        public ICharacterAbility Charisma { get; set; }
        public bool IsDead { get { return this.HitPoints < 1; } }

        public Character()
            : this(new CharacterInitializer())
        {
        }

        public Character(
            ICharacterInitializer characterInitializer)
        {
            _characterInitializer = characterInitializer;

            _characterInitializer.Initialize(this);
        }

        public bool Attack(int attackValue, ICharacter attackedCharacter)
        {
            if (attackValue >= attackedCharacter.ArmorClass)
            {
                attackedCharacter.HitPoints -= 1;

                if (attackValue == 20) // double damage on a critical hit, so take an extra hit point of damage
                    attackedCharacter.HitPoints -= 1;

                return true;
            }

            return false;
        }
    }
}
