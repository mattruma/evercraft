﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EverCraft.Tests
{
    [TestClass]
    public class CharacterTests
    {
        [TestMethod]
        public void ItHasName()
        {
            var character = new Character();

            Assert.IsNotNull(character.Name);
        }

        [TestMethod]
        public void ItHasNameThatCanBeSet()
        {
            var character = new Character();

            character.Name = "Stryder";

            Assert.AreEqual("Stryder", character.Name);
        }

        [TestMethod]
        public void ItHasAlignment()
        {
            var character = new Character();

            Assert.IsNotNull(character.Alignment);
        }

        [TestMethod]
        public void ItHasAlignmentThatDefaultsToGood()
        {
            var character = new Character();

            Assert.AreEqual(CharacterAlignment.Good, character.Alignment);
        }

        [TestMethod]
        public void ItHasAlignmentThatIsGood()
        {
            var character = new Character();

            character.Alignment = CharacterAlignment.Good;

            Assert.AreEqual(CharacterAlignment.Good, character.Alignment);
        }

        [TestMethod]
        public void ItHasAlignmentThatIsNeutral()
        {
            var character = new Character();

            character.Alignment = CharacterAlignment.Neutral;

            Assert.AreEqual(CharacterAlignment.Neutral, character.Alignment);
        }

        [TestMethod]
        public void ItHasAlignmentThatIsEvil()
        {
            var character = new Character();

            character.Alignment = CharacterAlignment.Evil;

            Assert.AreEqual(CharacterAlignment.Evil, character.Alignment);
        }

        [TestMethod]
        public void ItHasArmorClassThatDefaultsTo10()
        {
            var character = new Character();

            Assert.AreEqual(10, character.ArmorClass);
        }

        [TestMethod]
        public void ItHasHitPointsThatDefaultsTo5()
        {
            var character = new Character();

            Assert.AreEqual(5, character.HitPoints);
        }

        [TestMethod]
        public void ItCanAttack()
        {
            var character = new Character();
            var attackedCharacter = new Character();

            character.Attack(20, attackedCharacter);
        }

        [TestMethod]
        public void ItCanAttackAndMissWhenAttackValueIsLessedThanAttackedCharacterArmorClass()
        {
            var character = new Character();
            var attackedCharacter = new Character();

            attackedCharacter.ArmorClass = 10;

            var attackResult = character.Attack(9, attackedCharacter);

            Assert.IsFalse(attackResult);
        }

        [TestMethod]
        public void ItCanAttackAndHitWhenAttackValueIsEqualToAttackedCharacterArmorClass()
        {
            var character = new Character();
            var attackedCharacter = new Character();

            attackedCharacter.ArmorClass = 10;

            var attackResult = character.Attack(10, attackedCharacter);

            Assert.IsTrue(attackResult);
        }

        [TestMethod]
        public void ItCanAttackAndHitWhenAttackValueIsGreaterThanAttackedCharacterArmorClass()
        {
            var character = new Character();
            var attackedCharacter = new Character();

            attackedCharacter.ArmorClass = 10;

            var attackResult = character.Attack(11, attackedCharacter);

            Assert.IsTrue(attackResult);
        }

        [TestMethod]
        public void ItCanAttackAndHitAndCause1PointOfDamageToAttackedCharacter()
        {
            var character = new Character();
            var attackedCharacter = new Character();

            attackedCharacter.HitPoints = 5;
            attackedCharacter.ArmorClass = 10;

            character.Attack(10, attackedCharacter);

            Assert.AreEqual(4, attackedCharacter.HitPoints);
        }

        [TestMethod]
        public void ItCanAttackAndHitWithAttackValue20AndCausesDoubleDamageToAttackedCharacter()
        {
            var character = new Character();
            var attackedCharacter = new Character();

            attackedCharacter.HitPoints = 5;
            attackedCharacter.ArmorClass = 10;

            character.Attack(20, attackedCharacter);

            Assert.AreEqual(3, attackedCharacter.HitPoints);
        }

        [TestMethod]
        public void ItCanAttackAndKillAttackedCharacter()
        {
            var character = new Character();
            var attackedCharacter = new Character();

            attackedCharacter.HitPoints = 1;
            attackedCharacter.ArmorClass = 10;

            character.Attack(11, attackedCharacter);

            Assert.IsTrue(attackedCharacter.IsDead);
        }

        [TestMethod]
        public void ItCanBeDead()
        {
            var character = new Character();

            character.HitPoints = 0;

            Assert.IsTrue(character.IsDead);
        }

        [TestMethod]
        public void ItCanBeAlive()
        {
            var character = new Character();

            character.HitPoints = 1;

            Assert.IsFalse(character.IsDead);
        }

        [TestMethod]
        public void ItHasAbilities()
        {
            var character = new Character();

            Assert.IsNotNull(character.Strength);
            Assert.IsNotNull(character.Dexterity);
            Assert.IsNotNull(character.Constitution);
            Assert.IsNotNull(character.Wisdom);
            Assert.IsNotNull(character.Intelligence);
            Assert.IsNotNull(character.Charisma);
        }

        [TestMethod]
        public void ItHasAbilityThatDefaultsTo10()
        {
            var characterAbility = new CharacterAbility();

            Assert.AreEqual(10, characterAbility.Value);
        }

        [TestMethod]
        public void ItHasAbilityThatIsGreaterThanOrEqualTo1()
        {
            var characterAbility = new CharacterAbility();

            characterAbility.Value = 1;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ItHasAbilityThatIsGreaterThanOrEqualTo1ExpectException()
        {
            var characterAbility = new CharacterAbility();

            characterAbility.Value = 0;
        }

        [TestMethod]
        public void ItHasAbilityThatIsLessThanOrEqualTo20()
        {
            var characterAbility = new CharacterAbility();

            characterAbility.Value = 20;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ItHasAbilityThatIsLessThanOrEqualTo20ExpectedException()
        {
            var characterAbility = new CharacterAbility();

            characterAbility.Value = 21;
        }

        [TestMethod]
        public void ItHasAbilityModifier()
        {
            var characterAbility = new CharacterAbility();
            var abilityModifiers =
                new Dictionary<int, int>
                    {
                        {1, -5},
                        {2, -4},
                        {3, -4},
                        {4, -3},
                        {5, -3},
                        {6, -2},
                        {7, -2},
                        {8, -1},
                        {9, -1},
                        {10, 0},
                        {11, 0},
                        {12, 1},
                        {13, 1},
                        {14, 2},
                        {15, 2},
                        {16, 3},
                        {17, 3},
                        {18, 4},
                        {19, 4},
                        {20, 5},
                    };

            foreach (var abilityModifier in abilityModifiers)
            {
                characterAbility.Value = abilityModifier.Key;

                Assert.AreEqual(abilityModifier.Value, characterAbility.Modifier);
            }
        }
    }
}
